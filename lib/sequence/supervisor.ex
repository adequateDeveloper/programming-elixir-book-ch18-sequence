defmodule Sequence.Supervisor do
  use Supervisor


  # API

  def start_link(initial_number) do
    result = {:ok, sup} = Supervisor.start_link(__MODULE__, [initial_number])
    start_workers(sup, initial_number)
    result
  end

  # Supervisor starts the child worker process Stash passing it the initial sequence number.
  # Then supervisor starts the child supervisor SubSupervisor, passing it the pid of Stash
  def start_workers(sup, initial_number) do
    {:ok, stash} = Supervisor.start_child(sup, worker(Sequence.Stash, [initial_number]))
    Supervisor.start_child(sup, supervisor(Sequence.SubSupervisor, [stash]))
  end


  # Callbacks

  # Supervisor is now running, but has no children
  def init(_) do
    supervise [], strategy: :one_for_one
  end
end
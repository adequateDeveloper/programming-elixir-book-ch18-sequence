defmodule Sequence.Server do

  # Add OTP GenServer behavior to the module.
  use GenServer

  @vsn "0"

  # Examples:
  # ~$ iex -S mix
  # iex> Sequence.Server.next_number
  # iex> Sequence.Server.next_number
  # iex> Sequence.Server.increment_number 100
  # iex> Sequence.Server.next_number
  # iex> Sequence.Server.increment_number "cause it to crash"
  # iex> Sequence.Server.next_number
  # iex> Sequence.Server.next_number

  # API

  def start_link(stash_pid) do
    {:ok, _pid} = GenServer.start_link(__MODULE__, stash_pid, name: __MODULE__)
  end

  def next_number do
    GenServer.call __MODULE__, :next_number
  end

#  def set_number(new_number) do
#    GenServer.call __MODULE__, {:set_number, new_number}
#  end

  def increment_number(delta) do
    GenServer.cast __MODULE__, {:increment_number, delta}
  end

#  def get_status do
#    :sys.get_status __MODULE__
#  end


  # Callbacks

  # Examples:
  # ~$ iex -S mix
  # iex> { :ok, pid } = GenServer.start_link(Sequence.Server, 100, name: :seq)
  # iex> GenServer.call(:seq, :next_number)
  # iex> GenServer.call(:seq, {:set_number, 88})
  # iex> GenServer.cast(:seq, {:increment_number, 100})
  # iex> :sys.get_status :seq

  def init(stash_pid) do
    current_number = Sequence.Stash.get_value stash_pid
    { :ok, {current_number, stash_pid} }
  end

  def handle_call(:next_number, _from, {current_number, stash_pid}) do
    # Tell OTP to reply to the client with current state and provide new state to OTP to manage
    { :reply, current_number, {current_number+1, stash_pid} }
  end

#  def handle_call({:set_number, new_number}, _from, _current_number) do
#    # Tell OTP to reply to the client with the provided number and provide new state to OTP to manage
#    { :reply, new_number, new_number }
#  end

  def handle_cast({:increment_number, delta}, {current_number, stash_pid}) do
   # Tell OTP to not reply to the client and provide new state to OTP to manage
   { :noreply, {current_number + delta, stash_pid} }
  end

#  def format_status(_reason, [ _pdict, state ]) do
#    [data: [{'State', "My current state is '#{inspect state}', and I'm happy"}]]
#  end

  def terminate(_reason, {current_number, stash_pid}) do
    Sequence.Stash.save_value stash_pid, current_number
  end

end
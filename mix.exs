defmodule Sequence.Mixfile do
  use Mix.Project

  def project do
    [app: :sequence,
     version: "0.1.0",
     elixir: "~> 1.2.6",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [
      applications: [:logger],

       # Tell OTP the main entry point to the application.
       # OTP calls this module's start/2 function when it starts the application.
       mod: {Sequence, []},
       env: [initial_number: 456],

       # Lists the names that the application will register. To insure unique names across all loaded applications in
       # a node or cluster.
       registered: [ Sequence.Server ]
     ]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [
      {:exrm, "~> 1.0.8"}
    ]
  end
end
